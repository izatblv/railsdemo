Rails.application.routes.draw do
  root to: 'page#home'
  get '/selenium', to: 'page#selenium'
  get '/demo1', to: 'demo1#index'
  get '/demo1/admin', to: 'demo1#admin'
  get '/demo1/admin/posts', to: 'demo1#posts'
  get '/form', to: 'page#form'
  post '/form' => 'page#form'
  get '/users_reg', to: 'page#users_reg'
  # get '/etsdemo', to: 'page#etsdemo'
  # post '/etsdemo' => 'page#etsdemo'

  resources :microposts
  resources :users
  resources :demo1

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
