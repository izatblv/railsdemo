class User < ApplicationRecord
  has_many :microposts
  default_scope -> { order('created_at DESC') }
end
