class PageController < ApplicationController
  include ApplicationHelper
  def selenium
    @title="QA Selenium Automatic test"
    render layout: "application2"
  end
  def form
  end
  def home
  end
  def etsdemo
    @report=[]
    if params['commit']=="Go"
      start_ets
      login_ets
    end
    if params['checker']=='1'
      sent_mail_ets
    end
  end
end
